defmodule Cupboard.Server do
  @doc false
  use Agent

  @cupboard __MODULE__

  @moduledoc """
  Documentation for `Cupboard.Server` API.

  `Cupboard.Server` stores and retrieves state in a process.

  As is, `Cupboard.Server` is not supervised; the assumption
  being it will be used as a child component in a supervised application.
  """

  #######
  # API #
  #######

  @doc """
  Start the `Cupboard.Server` with an empty list
  as the default state, unless otherwise provided.

  ## Example

      iex>{:ok, pid} = Cupboard.Server.start_link
      iex> is_pid(pid)
      true

  """
  def start_link(initial_state \\ []) do
    Agent.start_link(fn -> initial_state end, name: @cupboard)
  end

  @doc """
  Retrieve the currently stored state

  ## Example

      iex>Cupboard.Server.start_link
      iex>Cupboard.Server.get
      []

      iex>Cupboard.Server.start_link {0,0}
      iex>Cupboard.Server.get
      {0,0}

  """
  def get, do: Agent.get(@cupboard, & &1)

  @doc """
  Update the currently stored state

  ## Example

      iex>Cupboard.Server.start_link
      iex>Cupboard.Server.get
      []
      iex>Cupboard.Server.update [1]
      iex>Cupboard.Server.get
      [1]

      iex>Cupboard.Server.start_link {0,0}
      iex>Cupboard.Server.get
      {0,0}
      iex>Cupboard.Server.update {0,1}
      iex>Cupboard.Server.get
      {0,1}

      defmodule State, do: defstruct number: 0

      iex>import State
      iex>Cupboard.Server.start_link %State{}
      iex>Cupboard.Server.get
      %State{number: 0}
      iex>Cupboard.Server.update %State{number: 1}
      iex>Cupboard.Server.get
      %State{number: 1}

  """
  def update(new_state),
    do: Agent.update(@cupboard, fn _old_state -> new_state end)
end
