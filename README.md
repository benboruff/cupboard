# Cupboard

**A simple application to maintain state in a separate process; perhaps for persisting the state of another application.**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `cupboard` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:cupboard, "~> 0.2.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/cupboard/api-reference.html](https://hexdocs.pm/cupboard/api-reference.html).
