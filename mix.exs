defmodule Cupboard.MixProject do
  use Mix.Project

  def project do
    [
      app: :cupboard,
      version: "0.2.0",
      elixir: "~> 1.9",
      description: description(),
      package: package(),
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      source_url: "https://gitlab.com/benboruff/cupboard"
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:ex_doc, "~> 0.21.1", only: :dev, runtime: false}
    ]
  end

  defp description do
    "A simple application to save and retrieve state in a process for persistence"
  end

  defp package do
    [
      licenses: ["MIT"],
      links: %{"GitLab" => "https://gitlab.com/benboruff/cupboard"}
    ]
  end
end
